# KG-1

RGB ↔ CMYK ↔ HSV converter

## Installation

```bash
pip install tk
```

## Usage

```bash
python3 main.py
```
