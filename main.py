import tkinter as tk
from tkinter.ttk import Scale, Button, Separator
from tkinter import BOTH, IntVar, DoubleVar, LEFT, HORIZONTAL, font, RAISED, colorchooser
from functools import partial


class App(tk.Tk):
    def __init__(self):
        super().__init__()

        self.color_lbl = tk.Label(self,width=20,height=6,bg="white",relief=RAISED)
        self.color_lbl.grid(column=1,row=0,columnspan=2, sticky='we')

        self.scale_R = Scale(self, from_=0, to=255, command=self.onScaleR,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_G = Scale(self, from_=0, to=255, command=self.onScaleG,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_B = Scale(self, from_=0, to=255, command=self.onScaleB,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_C = Scale(self, from_=0, to=100, command=self.onScaleC,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_M = Scale(self, from_=0, to=100, command=self.onScaleM,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_Y = Scale(self, from_=0, to=100, command=self.onScaleY,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_K = Scale(self, from_=0, to=100, command=self.onScaleK,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_H = Scale(self, from_=0, to=360, command=self.onScaleH,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_S = Scale(self, from_=0, to=100, command=self.onScaleS,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scale_V = Scale(self, from_=0, to=100, command=self.onScaleV,orient=HORIZONTAL, length=150, 
            cursor="tcross")

        self.scales=10
        self.sc_chars = ['R','G','B','C','M','Y','K','H','S','V']
        self.scales_lst = [self.scale_R, self.scale_G, self.scale_B, self.scale_C,
         self.scale_M, self.scale_Y, self.scale_K, self.scale_H, self.scale_S, self.scale_V]
        self.sc_values = [0]*self.scales
        self.sc_entries = [0]*self.scales

        for i in range(self.scales):
            tk.Label(self, text=self.sc_chars[i]).grid(row=i+1, column=0, padx=5)
            self.scales_lst[i].grid(row=i+1, column=1, pady=10)
            self.sc_values[i] = IntVar()
            self.sc_entries[i] = tk.Entry(self, textvariable=self.sc_values[i]).grid(row=i+1, column=2,
                sticky="E")

        for i in range(self.scales):
            self.sc_values[i].trace("w", lambda l, idx, mode, i=i:self.input_val(i))

        self.tracing_cmyk = True
        self.tracing_rgb = True
        self.tracing_hsv = True

        self.option = Button(self, text="Choose a color", command=self.color_ch).grid(row=12,column=1, columnspan=2)


    def set_val_and_scale(self, ind, val=None):
        if val is None:
            self.sc_values[ind].set(self.sc_values[ind].get())
            self.scales_lst[ind].set(self.sc_values[ind].get())
        else:
            self.sc_values[ind].set(val)
            self.scales_lst[ind].set(val)


    def input_val(self, val):
        try:
            self.set_val_and_scale(val)
        except:
            print('Input integer value')

    def onScaleR(self, val):
        print('onScaleR')
        self.R = int(self.scales_lst[0].get())
        self.sc_values[0].set(self.R)
        if self.tracing_rgb:
            self.trace_RGB1()

    def onScaleG(self, val):
        print('onScaleG')
        self.G = int(self.scales_lst[1].get())
        self.sc_values[1].set(self.G)
        if self.tracing_rgb:
            self.trace_RGB1()

    def onScaleB(self, val):
        print('onScaleB')
        self.B = int(self.scales_lst[2].get())
        self.sc_values[2].set(self.B)
        if self.tracing_rgb:
            self.trace_RGB1()

    def onScaleC(self, val):
        print('onScaleC')
        self.C = int(self.scales_lst[3].get())
        self.sc_values[3].set(self.C)
        if self.tracing_cmyk:
            self.trace_CMYK1()

    def onScaleM(self, val):
        print('onScaleM')
        print('tracing_cmyk',self.tracing_cmyk)
        print('tracing_rgb',self.tracing_rgb)
        print('tracing_hsv',self.tracing_hsv)

        self.M = int(self.scales_lst[4].get())
        self.sc_values[4].set(self.M)
        if self.tracing_cmyk:
            self.trace_CMYK1()

    def onScaleY(self, val):
        print('onScaleY')
        self.Y = int(self.scales_lst[5].get())
        self.sc_values[5].set(self.Y)
        if self.tracing_cmyk:
            self.trace_CMYK1()

    def onScaleK(self, val):
        print('onScaleK')
        self.K = int(self.scales_lst[6].get())
        self.sc_values[6].set(self.K)
        if self.tracing_cmyk:
            self.trace_CMYK1()

    def onScaleH(self, val):
        print('onScaleH')
        self.H = int(self.scales_lst[7].get())
        self.sc_values[7].set(self.H)
        if self.tracing_hsv:
            self.trace_HSV1()

    def onScaleS(self, val):
        print('onScaleS')
        self.S = int(self.scales_lst[8].get())
        self.sc_values[8].set(self.S)
        if self.tracing_hsv:
            self.trace_HSV1()

    def onScaleV(self, val):
        print('onScaleV')
        self.V = int(self.scales_lst[9].get())
        self.sc_values[9].set(self.V)
        if self.tracing_hsv:
            self.trace_HSV1()


    def trace_RGB1(self):
        #print("i'm in TRACE_RGB1")

        self.tracing_cmyk = False
        self.tracing_hsv = False

        rgb2hsv_val = self.rgb_to_hsv()
        for i in range(len(rgb2hsv_val)):
            self.set_val_and_scale(i+7, rgb2hsv_val[i])
      
        rgb2hcmyk_val= self.rgb_to_cmyk()
        for i in range(len(rgb2hcmyk_val)):
            self.set_val_and_scale(i+3, rgb2hcmyk_val[i])

        self.tracing_cmyk = True
        self.tracing_hsv = True


    def trace_CMYK1(self):
        #print("i'm in trace_CMYK1")

        self.tracing_rgb = False
        self.tracing_hsv = False

        cmyk2rgb_val = self.cmyk_to_rgb()
        for i in range(len(cmyk2rgb_val)):
            self.set_val_and_scale(i, cmyk2rgb_val[i])

        cmyk2hsv_val = self.cmyk_to_hsv(cmyk2rgb_val)
        for i in range(len(cmyk2hsv_val)):
            self.set_val_and_scale(i+7, cmyk2hsv_val[i])

        self.tracing_rgb = True
        self.tracing_hsv = True

    def trace_HSV1(self):
       # print("i'm in trace_HSV1")
        self.tracing_rgb = False
        self.tracing_cmyk = False

        hsv2rgb = self.hsv_to_rgb()
        for i in range(len(hsv2rgb)):
            self.set_val_and_scale(i, hsv2rgb[i])

        hsv2cmyk = self.hsv_to_cmyk(hsv2rgb)
        for i in range(len(hsv2cmyk)):
            self.set_val_and_scale(i+3, hsv2cmyk[i])

        self.tracing_rgb = True
        self.tracing_cmyk = True

    def color_ch(self):
        color = colorchooser.askcolor()
        for i in range(len(color[0])):
            self.sc_values[i].set(color[0][i])
            self.scales_lst[i].set(color[0][i])

    def rgb_to_hsv(self, rgb=None):
        R = int(self.scales_lst[0].get())
        G = int(self.scales_lst[1].get())
        B = int(self.scales_lst[2].get())

        self.set_lbl_color((R,G,B))

        if rgb is None:
            R2 = R/255
            G2 = G/255
            B2 = B/255
        else:
            R2 = rgb[0]/255
            G2 = rgb[1]/255
            B2 = rgb[2]/255

        Cmax = max(R2,G2,B2)
        Cmin = min(R2,G2,B2)
        delta_C = Cmax - Cmin

        h = 0
        s = 0
        v = 0
        if delta_C == 0:
            h = 0
        elif Cmax == R2:
            h = (60 * ((G2 - B2) / delta_C) + 360) % 360
        elif Cmax == G2:
            h = (60 * ((B2 - R2) / delta_C) + 120) % 360
        elif Cmax == B2:
            h = (60 * ((R2 - G2) / delta_C) + 240) % 360
     
        if Cmax == 0:
            s = 0
        else:
            s = (delta_C / Cmax) * 100
     
        v = Cmax * 100

        return (round(h), round(s), round(v))

    def rgb_to_cmyk(self, rgb=None):
        if rgb is None:
            R2 = int(self.scales_lst[0].get())/255
            G2 = int(self.scales_lst[1].get())/255
            B2 = int(self.scales_lst[2].get())/255
        else:
            R2 = rgb[0]/255
            G2 = rgb[1]/255
            B2 = rgb[2]/255

        K=1-max(R2,G2,B2)
        if (1-K)!=0:
            C=(1-R2-K)/(1-K)
            M=(1-G2-K)/(1-K)
            Y=(1-B2-K)/(1-K)

            res = (C,M,Y,K)
            return tuple(round(100*c) for c in res)
        else:
            return (0,0,0,0)

    def cmyk_to_rgb(self):
        C2, M2, Y2, K2 = [int(self.scales_lst[i].get())/100 for i in range(3,7)]
        R = 255 * (1-C2) * (1-K2)
        G = 255 * (1-M2) * (1-K2)
        B = 255 * (1-Y2) * (1-K2)

        res = (R, G, B)
        return tuple(round(c) for c in res)

    def set_lbl_color(self, rgb):
        #print('set_lbl_color',rgb)
        lbl_bg =  "#%02x%02X%02x"%(rgb[0], rgb[1], rgb[2])
        self.color_lbl.config(bg=lbl_bg)

    def cmyk_to_hsv(self, rgb):
        #cmyk_to_rgb()
        res = self.rgb_to_hsv(rgb)
        return res

    def hsv_to_rgb(self):
        h = int(self.scales_lst[7].get())
        s = int(self.scales_lst[8].get())
        v = int(self.scales_lst[9].get())

        s=s/100
        v=v/100
        c = v * s
        x = c * (1 - abs(((h/60.0) % 2) - 1))
        m = v - c
        
        if 0.0 <= h < 60:
            rgb = (c, x, 0)
        elif 60 <= h < 120:
            rgb = (x, c, 0)
        elif 120 <= h < 180:
            rgb = (0, c, x)
        elif 180 <= h < 240:
            rgb = (0, x, c)
        elif 240 <= h < 300:
            rgb = (x, 0, c)
        elif 300 <= h < 360:
            rgb = (c, 0, x)
            
        (R,G,B) = ((rgb[0]+m)*255, (rgb[1]+m)*255, (rgb[2]+m)*255)
        res = (round(R), round(G), round(B))
        self.set_lbl_color(res)

        return res


    def hsv_to_cmyk(self,rgb):
        #rgb_to_cmyk()
        res = self.rgb_to_cmyk(rgb)
        return res


if __name__ == "__main__":
    root = App()
    default_font = tk.font.nametofont("TkDefaultFont")
    default_font.configure(size=14)
    root.geometry("510x650")
    root.option_add("*Font", default_font)
    root.mainloop()